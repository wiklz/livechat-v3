import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'
import { Style } from '../lib/react-shade'
import { RootState } from '../store'
import styles from './styles'

interface Props {
  isMobile: boolean
  children?: any
}

const WidgetContainer: React.FC<Props> = ({ isMobile, children = null }: Props) => {
  const isCollapsed = useSelector((state: RootState) => !state.application.widgetOpen)
  const theme = useSelector((state: RootState) => state.settings.theme)
  const right = useSelector((state: RootState) => state.settings.position.right)
  const bottom = useSelector((state: RootState) => state.settings.position.bottom)
  const horizontal = useSelector((state: RootState) => state.settings.appearance[isMobile ? 'mobile' : 'desktop'].horizontal)

  const classes = useMemo(() => {
    if (isMobile) {
      return [
        'widget-mobile',
        isCollapsed ? 'widget-mobile--collapsed' : 'widget-mobile--open',
        theme === 'dark' ? 'widget-mobile--dark' : 'widget-mobile--light',
        right ? 'widget-mobile--right' : 'widget-mobile--left',
        bottom ? 'widget-mobile--bottom' : 'widget-mobile--center',
        horizontal ? 'widget-mobile--horizontal' : 'widget-mobile--vertical'
      ].join(' ')
    }
    return [
      'widget',
      isCollapsed ? 'widget--collapsed' : 'widget--open',
      theme === 'dark' ? 'widget--dark' : 'widget--light',
      right ? 'widget--right' : 'widget--left',
      bottom ? 'widget--bottom' : 'widget--center',
      horizontal ? 'widget--horizontal' : 'widget--vertical'
    ].join(' ')
  }, [isMobile, isCollapsed, theme, right, bottom, horizontal])

  return (
    <>
      <Style>{styles}</Style>
      <div data-testid="widget-container" className={classes}>
        {children}
      </div>
    </>
  )
}

export default WidgetContainer
