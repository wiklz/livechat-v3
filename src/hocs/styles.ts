const styles = {
  '.widget': {
    position: 'absolute',
    display: 'flex',
    boxShadow: '0px 5px 25px rgba(0, 0, 0, 0.15)',
    height: 'auto',
    width: 'auto',
    transition: 'all 0.1s linear'
  },
  '.widget--dark': {
    backgroundColor: '#0e1621'
  },
  '.widget--light': {
    backgroundColor: '#ffffff'
  },
  '.widget--left': {
    left: 0
  },
  '.widget--right': {
    right: 0
  },
  '.widget--bottom': {
    bottom: 0
  },
  '.widget--center': {
    top: 0
  },
  '.widget--open': {
    borderRadius: '14px'
  },
  '.widget--collapsed': {
    borderRadius: '37px'
  },
  '.widget--collapsed:hover': {
    transform: 'scale(1.18)'
  },
  '.widget--horizontal': {
    flexFlow: 'row',
    height: '74px',
    width: 'auto',
    padding: '0 10px'
  },
  '.widget--horizontal.widget--open': {
    width: 'auto'
  },
  '.widget--horizontal.widget--collapsed': {
    width: '74px',
    padding: 0
  },
  '.widget--vertical': {
    flexFlow: 'column',
    width: '74px',
    height: 'auto',
    padding: '10px 0'
  },
  '.widget--vertical.widget--open': {
    height: 'auto'
  },
  '.widget--vertical.widget--collapsed': {
    height: '74px',
    padding: 0
  },
  '.widget--center.widget--vertical': {
    flexFlow: 'column-reverse'
  },
  '.widget--horizontal.widget--left': {
    flexFlow: 'row-reverse'
  },
  '.widget-mobile': {
    position: 'fixed',
    display: 'flex',
    boxShadow: '0px -4px 10px rgba(0, 0, 0, 0.15)',
    transition: 'transform 0.2s linear',
    bottom: 0
  },
  '.widget-mobile--dark': {
    backgroundColor: '#0e1621'
  },
  '.widget-mobile--light': {
    backgroundColor: '#ffffff'
  },
  '.widget-mobile--horizontal': {
    flexFlow: 'row',
    height: '64px',
    width: '100%',
    padding: '0 10px',
    overflowX: 'auto'
  },
  '.widget-mobile--horizontal.widget-mobile--collapsed': {
    transform: 'translateY(64px)'
  },
  '.widget-mobile--vertical': {
    flexFlow: 'column',
    width: '64px',
    height: 'auto',
    padding: '10px 0',
    justifyContent: 'center'
  },
  '.widget-mobile--vertical.widget-mobile--right': {
    borderRadius: '14px 0 0 0'
  },
  '.widget-mobile--vertical.widget-mobile--right.widget-mobile--collapsed': {
    transform: 'translateX(64px)'
  },
  '.widget-mobile--vertical.widget-mobile--left': {
    borderRadius: '0 14px 0 0'
  },
  '.widget-mobile--vertical.widget-mobile--left.widget-mobile--collapsed': {
    transform: 'translateX(-64px)'
  },
  '.widget-mobile--left': {
    left: 0
  },
  '.widget-mobile--right': {
    right: 0
  },
  '.widget-mobile--collapsed': {
    boxShadow: 'none'
  }
}

export default styles
