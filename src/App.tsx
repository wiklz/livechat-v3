import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from './store'
import OnlineChat from './containers/OnlineChat'
import SvgSprite from './components/UI/SvgSprite'
import Widget from './containers/Widget'

const App: React.FC = () => {
  const [isMobile, setIsMobile] = useState(window.innerWidth < 1024)

  const widgetEnabled = useSelector((state: RootState) => state.settings.widgetEnabled)
  const position = useSelector((state: RootState) => state.settings.position)
  const settingsLoaded = useSelector((state: RootState) => state.settings.settingsLoaded)
  const clientPhone = useSelector((state: RootState) => state.chat.client_phone)
  const showOnlineChat = useSelector((state: RootState) => state.settings.messengers.find((messenger) => messenger.id === 'widget'))

  const dispatch = useDispatch()

  useEffect(() => {
    const resize = () => {
      setIsMobile(window.innerWidth < 1024)
      window.chat24_isMobile = window.innerWidth < 1024
    }
    window.addEventListener('resize', resize, { passive: true })

    dispatch({
      type: 'widget/initSettings'
    })
    return () => {
      window.removeEventListener('resize', resize)
    }
  }, [])

  useEffect(() => {
    if (settingsLoaded && showOnlineChat && clientPhone) {
      dispatch({
        type: 'chat/initMessages'
      })
      dispatch({
        type: 'chat/initSocketConnection'
      })
    }
  }, [settingsLoaded, showOnlineChat, clientPhone])

  const styles: React.CSSProperties = {
    position: window.chat24_target ? 'absolute' : 'fixed',
    [position.right ? 'right' : 'left']: `${position.edgePadding}${window.chat24_target ? '%' : 'vw'}`,
    [position.bottom ? 'bottom' : 'top']: position.bottom
      ? `${position.edgePadding}${window.chat24_target ? '%' : 'vh'}`
      : 'calc(50% - 37px)'
  }

  return settingsLoaded && widgetEnabled ? (
    <>
      <SvgSprite />
      <div className="chat24-container" style={styles}>
        {showOnlineChat && <OnlineChat isMobile={isMobile} />}
        <Widget isMobile={isMobile} />
      </div>
    </>
  ) : null
}

export default App
