import axios, { AxiosResponse } from 'axios'
import jwtDecode from 'jwt-decode'
import { JsonObjectExpression } from 'typescript'

const VERIFY_URL = `${window.chat24_url}/widget/verify_jwt`

export const decodeJWT = (ticket: string): any => jwtDecode(ticket)

export const isJWTvalid = (json: JsonObjectExpression): Promise<AxiosResponse<any>> => {
  const payload = {
    data: json
  }
  const response = axios.post(VERIFY_URL, payload)
  return response
}
