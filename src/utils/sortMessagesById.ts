import ChatMessage from '../models/ChatMessage'

export default (messages: ChatMessage[]) => (messages ? messages.sort((a, b) => b.id - a.id) : [])
