import Cookies from 'js-cookie'
import axios from 'axios'

const utmParams = [
  'utm_source',
  'utm_medium',
  'utm_campaign',
  'utm_content',
  'utm_term'
]

export const getURL = () => window.location.origin + window.location.pathname

export const getGoogleId = () => (Cookies.get('_ga') === undefined ? null : Cookies.get('_ga'))

export const getYandexId = () => (Cookies.get('_ym_uid') === undefined ? null : Cookies.get('_ym_uid'))

export const getRoistatVisit = () => (Cookies.get('roistat_visit') === undefined ? null : Cookies.get('roistat_visit'))

export const getComagicId = () => (window.Comagic ? window.Comagic.getCredentials().visitor_id : null)

const getUTMValue = (inputParameter: string) => {
  const utmQuery = decodeURIComponent(window.location.search.substring(1))
  const utmVariables = utmQuery.split('&')

  let value = ''

  utmVariables.forEach((variable) => {
    const [paramName, paramValue] = variable.split('=')
    if (paramName === inputParameter) {
      value = paramValue
    }
  })
  return value
}

export const getUTM = () => {
  let utm = ''
  utmParams.forEach((param) => {
    const pValue = getUTMValue(param)
    if (pValue !== null && pValue !== '' && pValue !== undefined) {
      utm += `&${param}=${pValue}`
    }
  })
  return utm
}

export const sendURLparams = (transportId: number, clickId: string, activeChannel: number | null) => {
  const payload = {
    transport: transportId,
    url: getURL(),
    utm: getUTM(),
    google_id: getGoogleId(),
    yandex_id: getYandexId(),
    comagic_Id: getComagicId(),
    roistat_visit: getRoistatVisit(),
    click_id: clickId,
    channel_id: activeChannel
  }
  axios.post(`${window.chat24_url}/click`, payload)
}

export const parsedUtmForAnalytics = () => {
  const utm = getUTM()
  const source = getUTMValue('utm_source')
  const medium = getUTMValue('utm_medium')
  const name = getUTMValue('utm_campaign')
  const term = getUTMValue('utm_term')
  const content = getUTMValue('utm_content')
  const custom = utm.substr(1)

  return {
    source, medium, name, term, content, custom
  }
}

export const sendToGoogleAnalytics = (counterId: number, dimensionId: number, transport: string) => {
  const parsedUtm = parsedUtmForAnalytics()
  if (window.gtag) {
    window.gtag(
      'config', counterId, {
        [`dimension${dimensionId}`]: transport,
        page_path: window.location.pathname,
        campaign: { parsedUtm }
      },
    )
  }
}

export const sendToYandexMetrica = (counterId: number, dimension: string, transport: string) => {
  if (window.ym) {
    window.ym(counterId, 'hit', window.location.pathname + window.location.search, {
      title: document.title,
      referer: window.location.hostname,
      params: { [dimension]: transport }
    })
  }
}
