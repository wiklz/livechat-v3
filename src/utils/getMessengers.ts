const getMessengers = (arr: { type: string; url?: string }[]) => {
  const messengers = arr.map((el) => {
    switch (el.type) {
      case 'modem':
        return {
          id: 'modem',
          dbId: 1,
          name: 'SMS'
        }
      case 'whatsapp':
        return {
          id: 'whatsapp',
          dbId: 2,
          name: 'WhatsApp',
          url: el.url ? el.url : '#'
        }
      case 'telegram':
        return {
          id: 'telegram',
          dbId: 3,
          name: 'Telegram',
          url: el.url ? el.url : '#'
        }
      case 'viber':
        return {
          id: 'viber',
          dbId: 4,
          name: 'Viber',
          url: el.url ? el.url : '#'
        }
      case 'facebook':
        return {
          id: 'facebook',
          dbId: 5,
          name: 'Facebook',
          url: el.url ? el.url : '#'
        }
      case 'external':
        return {
          id: 'external',
          dbId: 6,
          name: 'External channel'
        }
      case 'widget':
        return {
          id: 'widget',
          dbId: 7,
          name: 'Online-chat'
        }
      case 'vk':
        return {
          id: 'vk',
          dbId: 8,
          name: 'VK',
          url: el.url ? el.url : '#'
        }
      case 'viber_business':
        return {
          id: 'viber_business',
          dbId: 9,
          name: 'Viber Business',
          url: el.url ? el.url : '#'
        }
      case 'system':
        return {
          id: 'system',
          dbId: 10,
          name: 'SMS'
        }
      case 'viber_public':
        return {
          id: 'viber_public',
          dbId: 11,
          name: 'Viber Public',
          url: el.url ? el.url : '#'
        }
      case 'ok':
        return {
          id: 'ok',
          dbId: 12,
          name: 'ОК',
          url: el.url ? el.url : '#'
        }
      case 'instagram':
        return {
          id: 'instagram',
          dbId: 13,
          name: 'Instagram',
          url: el.url ? el.url : '#'
        }
      case 'insta_i2crm':
        return {
          id: 'insta_i2crm',
          dbId: 14,
          name: 'Instagram',
          url: el.url ? el.url : '#'
        }
      case 'insta_comment':
        return {
          id: 'insta_comment',
          dbId: 15,
          name: 'Insta comment',
          url: el.url ? el.url : '#'
        }
      case 'twitter':
        return {
          id: 'twitter',
          dbId: 16,
          name: 'Twitter',
          url: el.url ? el.url : '#'
        }
      case 'skype':
        return {
          id: 'skype',
          dbId: 17,
          name: 'Skype',
          url: el.url ? el.url : '#'
        }
      case 'vox_implant':
        return {
          id: 'vox_implant',
          dbId: 18,
          name: 'VoxImplant',
          url: el.url ? el.url : '#'
        }
      case 'email':
        return {
          id: 'email',
          dbId: 19,
          name: 'Email',
          url: el.url ? el.url : '#'
        }
      case 'wechat':
        return {
          id: 'wechat',
          dbId: 20,
          name: 'Wechat',
          url: el.url ? el.url : '#'
        }
      case 'yandex_dialogs':
        return {
          id: 'yandex_dialogs',
          dbId: 21,
          name: 'Yandex.Dialogs',
          url: el.url ? el.url : '#'
        }
      case 'wa_clickatell':
        return {
          id: 'wa_clickatell',
          dbId: 22,
          name: 'WhatsApp Clickatell',
          url: el.url ? el.url : '#'
        }
      case 'wa_infobip':
        return {
          id: 'wa_infobip',
          dbId: 23,
          name: 'WhatsApp Infobip',
          url: el.url ? el.url : '#'
        }
      case 'wa_cm':
        return {
          id: 'wa_cm',
          dbId: 24,
          name: 'WhatsApp CM',
          url: el.url ? el.url : '#'
        }
      case 'wa_tr':
        return {
          id: 'wa_tr',
          dbId: 25,
          name: 'WhatsApp SMS Transport',
          url: el.url ? el.url : '#'
        }
      case 'wa_wavy':
        return {
          id: 'wa_wavy',
          dbId: 26,
          name: 'WhatsApp Wavy',
          url: el.url ? el.url : '#'
        }
      case 'viber_infobip':
        return {
          id: 'viber_infobip',
          dbId: 27,
          name: 'Viber Infobip',
          url: el.url ? el.url : '#'
        }
      case 'insta_local':
        return {
          id: 'insta_local',
          dbId: 28,
          name: 'Insta local',
          url: el.url ? el.url : '#'
        }
      case 'tg_user':
        return {
          id: 'tg_user',
          dbId: 29,
          name: 'Telegram User',
          url: el.url ? el.url : '#'
        }
      case 'wa_botmaker':
        return {
          id: 'wa_botmaker',
          dbId: 30,
          name: 'WhatsApp Botmaker',
          url: el.url ? el.url : '#'
        }
      case 'viber_tr':
        return {
          id: 'viber_tr',
          dbId: 32,
          name: 'Viber SMS Transport',
          url: el.url ? el.url : '#'
        }
      case 'wa_dialog':
        return {
          id: 'wa_dialog',
          dbId: 33,
          name: 'WhatsApp Dialog',
          url: el.url ? el.url : '#'
        }

      default:
        return {
          id: 'system',
          dbId: 10,
          name: 'SMS'
        }
    }
  })

  return messengers.filter(
    (el) => el.id !== 'system'
      && el.id !== 'sms'
      && el.id !== 'external'
      && el.id !== 'modem'
  )
}

export default getMessengers
