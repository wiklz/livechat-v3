import _ from 'lodash'

class I18n {
  messages: any;

  constructor () {
    this.messages = {}
  }

  load (messages: any) {
    this.messages = messages
  }

  t (code: string, params = {}) {
    let result = this.messages[code]
    if (_.isEmpty(result)) return code
    if (!_.isEmpty(params)) {
      _.each(params, (value, key) => {
        const pattern = new RegExp(`%{${key}}`, 'g')
        result = result.replace(pattern, value)
      })
    }
    return result
  }
}

export default new I18n()
