import {
  sendURLparams,
  sendToGoogleAnalytics,
  sendToYandexMetrica
} from './utmTracking'

interface Transport {
    id: string;
    url?: string | undefined;
    dbId: number;
    name: string;
}

interface UrlTracking {
    enabled: boolean;
    url_field: number;
    utm_field: number;
    send_url_to_chat: boolean;
    [key: string]: string | number | boolean;
}

const buildRef = (transport:Transport, urlTracking: UrlTracking, activeChannel: number | null) => {
  if (!urlTracking || !urlTracking.enabled) {
    return transport.url
  }
  const clickId = (100000 + Math.round(Math.random() * 899999)).toString()
  const clickParam = `clk-${clickId}`
  let ref
  switch (transport.name) {
    case 'Telegram':
      ref = 'start='
      break
    case 'Facebook':
      ref = 'ref='
      break
    case 'Viber Public':
      ref = 'context='
      break
    default:
      console.log(`${transport.name} is not supported in switch case`)
  }

  ref = `${ref}${clickParam}`
  let url = transport.url || ''

  if (ref) {
    url += url.indexOf('?') >= 0 ? '&' : '?'
    url += ref
  }

  sendURLparams(transport.dbId, clickId, activeChannel)
  if (urlTracking.send_to_google_analytics) {
    sendToGoogleAnalytics(
      urlTracking.google_analytics_id as number,
      urlTracking.google_analytics_dimension_id as number,
      transport.name
    )
  }
  if (urlTracking.send_to_yandex_metrica) {
    sendToYandexMetrica(
      urlTracking.yandex_metrica_id as number,
      urlTracking.yandex_metrica_dimension_name as string,
      transport.name
    )
  }
  return url
}
export default buildRef
