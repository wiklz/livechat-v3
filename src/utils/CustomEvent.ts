const customEventBuilder = (name: string, payload?: any) => new CustomEvent(name, { detail: payload })
export default customEventBuilder
