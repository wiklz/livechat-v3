import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { store } from './store/index'
import App from './App'
import reportWebVitals from './reportWebVitals'
import './styles/index.scss'

// window.addEventListener('init_widget', () => {
//   const el = document.createElement('chat-24')
//   if (window.chat24_target) {
//     const targetEl = document.querySelector(`#${window.chat24_target}`)
//     if (targetEl) {
//       targetEl.appendChild(el)
//     }
//   } else {
//     document.body.appendChild(el)
//   }

//   /* eslint-disable class-methods-use-this */
//   class Chat24 extends HTMLElement {
//     connectedCallback () {
//       const mountPoint = el
//         .attachShadow({ mode: 'open' })
//         .appendChild(document.createElement('div'))
//       if (mountPoint) {
//         ReactDOM.render(
//           <React.StrictMode>
//             <Provider store={store}>
//               <App />
//             </Provider>
//           </React.StrictMode>,
//           mountPoint as Element
//         )
//       }
//     }
//   }
//   customElements.define('chat-24', Chat24)
// })
// /* eslint-enable class-methods-use-this */

// window.dispatchEvent(new CustomEvent('can_init'))

const mount = (el: Element) => {
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={store}>
        <App />
      </Provider>
    </React.StrictMode>,
    el
  )
}

let chat24RootEl = document.querySelector('#chat24-root')

if (!chat24RootEl) {
  chat24RootEl = document.createElement('div')
  chat24RootEl.setAttribute('id', 'chat24-root')
  document.body.appendChild(chat24RootEl)
}

mount(chat24RootEl)

reportWebVitals()
