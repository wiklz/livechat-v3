export interface SettingsState {
  client_phone: null | string;
  activeChannel: number | null;
  settingsLoaded: boolean;
  // Widget settings
  is_phone: boolean;
  phone_type: 0 | 1;
  phone_sip_link: string;
  phone_number: string;
  widgetEnabled: boolean;
  messengers: { id: string; url?: string; dbId: number; name: string }[];
  theme: 'light' | 'dark';
  startBtn: {
    color: string;
    active: 0 | 1 | 2 | 3 | 4;
    buttons: {
      id: 0 | 1 | 2 | 3 | 4;
      url?: string;
      fullSize?: string | null;
    }[];
  };
  position: {
    bottom: boolean;
    right: boolean;
    edgePadding: number;
  };
  appearance: {
    mobile: {
      enabled: boolean;
      collapsed: boolean;
      horizontal: boolean;
    };
    desktop: {
      enabled: boolean;
      collapsed: boolean;
      horizontal: boolean;
    };
  };
  poweredBy: {
    text: string | null;
    link: string | null;
    is_show_powered_by: boolean;
  };
  // Online-chat settings
  autoOpen: {
    active: boolean;
    delay: string;
  };
  themeColor: string;
  chatTitle: string;
  headerInfo: {
    avatar: boolean;
    name: boolean;
    answer: boolean;
    isTyping: boolean;
  };
  operator: {
    name: string;
    avatar: string;
    time: number;
    isTyping: boolean;
    isOnline: boolean;
    muted: boolean;
    url_prefix?: string;
  };
  allowFirstMsg: boolean;
  onlineForm: {
    show: boolean;
    fields: {
      id: string;
      custom: boolean;
      enabled: boolean;
      required: boolean;
      type?: string;
      name: {
        en: string;
        [key: string]: string;
      };
      customID?: number;
    }[];
  };
  offlineForm: {
    show: boolean;
    formName: string;
    description: string;
    fields: {
      id: string;
      custom: boolean;
      enabled: boolean;
      required: boolean;
      type?: string;
      name: {
        en: string;
        [key: string]: string;
      };
      customID?: number;
    }[];
  };
  transportsSettings: {
    yandex_dialogs?: {
      code: string;
    };
  } | null;
  chat_center: {
    status: boolean;
  };
  urlTracking: {
    enabled: boolean;
    url_field: number;
    utm_field: number;
    send_url_to_chat: boolean;
    [key: string]: string | number | boolean;
};
  JWTversion: '2' | '3';
  JWTjson: string;
  JWTjsonPayload: null | string;
}
