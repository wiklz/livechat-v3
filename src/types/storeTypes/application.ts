export interface ApplicationState {
    widgetAutoOpen: boolean;
    widgetOpen: boolean;
    viberOpen: boolean;
    authSubmitted: boolean;
    canPerformOpenChatEvent: boolean;
    chatOpen: null | boolean;
    viberInfo: {
        url: null | string;
        transport: null | string;
    }
}
