import ChatMessage from '../../models/ChatMessage'

export interface ChatState {
  jwtSaved: boolean;
  showEmoji: boolean;
  isMuted: boolean;
  client_phone: null | string;
  activeChannel: null | number;
  new_messages: number[];
  messages: ChatMessage[];
  messageInput: {
    text: string;
  };
  user: {
    isSendingAuthForm: boolean;
    isAuthorized: boolean;
    name: string;
    phone: string;
  };
  botMenu: {
    show: boolean;
    options: {
      id: number;
      title: string;
      action: string;
    }[];
  };
  modals: {
    attachment: {
      show: boolean;
      file: null | File;
      url?: null | string;
      link?: null | string;
      name: null | string;
      type: null | 'image' | 'file';
      size: null | number;
      text: null | number;
    };
  };
}
