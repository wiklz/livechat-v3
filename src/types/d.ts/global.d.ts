export {}
declare global {
  export interface Window {
    chat24_token: string;
    chat24_url: string;
    chat24_socket_url: string;
    chat24_show_new_wysiwyg: boolean;
    chat24_static_files_domain: string;
    chat24_target: string;
    lang: string;
    chat24_isMobile: boolean | undefined;
    JWT: 'enabled' | 'disabled' | undefined;
    JWT_payload_userId_path: string;
    JWT_hidden_field_id: string;
    Comagic: any;
    gtag: any;
    ym: any;
  }
}
