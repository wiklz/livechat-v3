export default class ChatMessage {
  constructor (
    readonly id: number,
    readonly type: number,
    readonly text: string,
    readonly date: number | string,
    readonly attachments: {
      id: number;
      link: string;
      original_file_name: null | string;
      content_type: string;
      file_size?: number;
    }[],
    public status: string,
    public read: boolean,
    public extra_data?: {
      url?: string;
      utm?: string;
      google_id?: null | number;
      yandex_id?: null | number;
      comagic_id?: null | number;
      roistat_visit?: any;
      keyboard?: {
        text: string;
        type: string;
        [key: string]: any;
      }[];
    }
  ) {}
}
