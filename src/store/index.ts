import {
  combineReducers,
  configureStore,
  getDefaultMiddleware
} from '@reduxjs/toolkit'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger'
import settings from './reducers/settingsReducer'
import chat from './reducers/chatSettings'
import application from './reducers/applicationSettings'
import sagas from './sagas'

const rootReducer = combineReducers({ settings, chat, application })

export type RootState = ReturnType<typeof rootReducer>;

const sagaMiddleware = createSagaMiddleware()

export const store = configureStore({
  reducer: rootReducer,
  middleware: [
    ...getDefaultMiddleware({ thunk: false }),
    ...[sagaMiddleware, logger]
  ],
  devTools: process.env.NODE_ENV !== 'production'
})

sagaMiddleware.run(sagas)

export default rootReducer
