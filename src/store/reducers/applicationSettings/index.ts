import { createSlice } from '@reduxjs/toolkit'
import { ApplicationState } from '../../../types/storeTypes/application'

const initialState: ApplicationState = {
  widgetAutoOpen: true,
  widgetOpen: false,
  chatOpen: null,
  viberOpen: false,
  authSubmitted: false,
  canPerformOpenChatEvent: true,
  viberInfo: {
    url: null,
    transport: null
  }
}

const applicationSlice = createSlice({
  name: 'application',
  initialState,
  reducers: {
    toggleWidgetAutoOpen: (state) => {
      state.widgetAutoOpen = !state.widgetAutoOpen
    },
    toggleWidgetOpen: (state) => {
      state.widgetOpen = !state.widgetOpen
    },
    setWidgetOpen: (state, { payload }) => {
      state.widgetOpen = payload
    },
    toggleChatOpen: (state) => {
      state.chatOpen = !state.chatOpen
    },
    toggleViberOpen: (state) => {
      state.viberOpen = !state.viberOpen
    },
    toggleAuthSubmitted: (state) => {
      state.authSubmitted = !state.authSubmitted
    },
    toggleCanPerformOpenChatEvent: (state) => {
      state.canPerformOpenChatEvent = !state.canPerformOpenChatEvent
    },
    changeViberInfo: (state, { payload }) => {
      state.viberInfo = payload
    },
    closeAll: (state) => {
      state.widgetOpen = false
      state.chatOpen = false
      state.viberOpen = false
      state.viberInfo = {
        url: null,
        transport: null
      }
    }
  }
})

export default applicationSlice.reducer

export const {
  toggleWidgetAutoOpen,
  toggleWidgetOpen,
  setWidgetOpen,
  toggleChatOpen,
  toggleViberOpen,
  toggleAuthSubmitted,
  toggleCanPerformOpenChatEvent,
  changeViberInfo,
  closeAll
} = applicationSlice.actions
