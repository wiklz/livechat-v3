import { createSlice } from '@reduxjs/toolkit'
import { SettingsState } from '../../../types/storeTypes/settings'
import setWidgetAction from './setWidgetAction'
import setChatAction from './setChatAction'
import customEvent from '../../../utils/CustomEvent'

const initialState: SettingsState = {
  client_phone: null,
  activeChannel: null,
  settingsLoaded: false,
  // Widget settings
  is_phone: false,
  phone_type: 0,
  phone_sip_link: '',
  phone_number: '',
  widgetEnabled: false,
  messengers: [],
  theme: 'light',
  startBtn: {
    color: '#283CFA',
    active: 0,
    buttons: [
      {
        id: 0
      },
      {
        id: 1
      },
      {
        id: 2
      },
      {
        id: 3
      },
      {
        id: 4,
        url: undefined,
        fullSize: null
      }
    ]
  },
  position: {
    bottom: true,
    right: true,
    edgePadding: 5
  },
  appearance: {
    mobile: {
      enabled: true,
      collapsed: true,
      horizontal: false
    },
    desktop: {
      enabled: true,
      collapsed: true,
      horizontal: false
    }
  },
  poweredBy: {
    text: null,
    link: null,
    is_show_powered_by: false
  },
  // Online-chat settings
  autoOpen: {
    active: true,
    delay: '0'
  },
  themeColor: '#283CFA',
  chatTitle: '',
  headerInfo: {
    avatar: true,
    name: true,
    answer: false,
    isTyping: false
  },
  operator: {
    name: '',
    avatar: '',
    time: 5,
    isTyping: false,
    isOnline: false,
    muted: false
  },
  allowFirstMsg: false,
  onlineForm: {
    show: false,
    fields: []
  },
  offlineForm: {
    show: false,
    formName: '',
    description: '',
    fields: []
  },
  transportsSettings: null,
  chat_center: {
    status: false
  },
  urlTracking: {
    enabled: false,
    url_field: 4,
    utm_field: 5,
    send_url_to_chat: false
  },
  JWTversion: '2',
  JWTjson: 'null',
  JWTjsonPayload: null
}

const settingsSlice = createSlice({
  name: 'widget',
  initialState,
  reducers: {
    setWidgetSettings: setWidgetAction,
    setChatSettings: setChatAction,
    initSuccess: (state) => {
      document.dispatchEvent(customEvent('initialize_widget', null))
      state.settingsLoaded = true
    },
    changeChatCenterStatus: (state, { payload }) => {
      state.chat_center.status = payload
    },
    changeOperatorInfo: (state, { payload }) => {
      state.operator = payload
    },
    changeOperatorTyping: (state, { payload }) => {
      state.headerInfo.isTyping = payload
    }
  }
})
export default settingsSlice.reducer
export const {
  setWidgetSettings,
  setChatSettings,
  initSuccess,
  changeChatCenterStatus,
  changeOperatorInfo,
  changeOperatorTyping
} = settingsSlice.actions
