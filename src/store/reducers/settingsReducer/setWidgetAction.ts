import { SettingsState } from '../../../types/storeTypes/settings'
import { decodeJWT } from '../../../utils/JWT'

const setWidgetSettings = (
  state: SettingsState,
  {
    payload
  }: {
    payload: any;
    type: string;
  }
):void => {
  const {
    is_phone = false,
    available = false,
    show_roll_mob = false,
    show_roll_desk = true,
    phone_type = 0,
    phone_sip_link = '',
    phone_number = '',
    platform = 'all',
    widget_theme = 'light',
    icon_location_mob = 2,
    icon_location_desk = 2,
    def_icon = 0,
    def_icon_color = '#283CFA',
    custom_icon = null,
    custom_icon_full = null,
    horizontal_position = 2,
    vertical_position = 2,
    margin = 5,
    powered_by = {
      text: '',
      link: ''
    },
    widget_transports_settings = {}
  } = payload.data

  const {
    messengers, url_tracking, active_channel, chat_center
  } = payload

  const jwtInput = window.document.getElementById(
    window.JWT_hidden_field_id
  ) as HTMLInputElement | null

  let JWTjson: any = null
  if (jwtInput) {
    JWTjson = window.JWT_hidden_field_id !== undefined
      ? JSON.parse(jwtInput.value)
      : null
  }

  if (!available) {
    console.log('WIDGET IS DISABLED. PLEASE ENABLE IT ON SETTINGS PAGE.')
  }

  state.widgetEnabled = Boolean(available)
  state.is_phone = is_phone
  state.phone_type = phone_type
  state.phone_sip_link = phone_sip_link
  state.phone_number = phone_number
  state.theme = widget_theme
  state.appearance = {
    ...state.appearance,
    mobile: {
      ...state.appearance.mobile,
      enabled: platform === 'all' || platform === 'mobile',
      collapsed: !show_roll_mob,
      horizontal: icon_location_mob === 1
    },
    desktop: {
      ...state.appearance.desktop,
      enabled: platform === 'all' || platform === 'desktop',
      collapsed: !show_roll_desk,
      horizontal: icon_location_desk === 1
    }
  }
  state.activeChannel = active_channel
  state.chat_center = chat_center
  state.urlTracking = url_tracking
  state.startBtn = {
    ...state.startBtn,
    active: def_icon,
    color: def_icon_color,
    buttons: [
      {
        id: 0
      },
      {
        id: 1
      },
      {
        id: 2
      },
      {
        id: 3
      },
      {
        id: 4,
        url: custom_icon,
        fullSize: custom_icon_full
      }
    ]
  }
  state.position = {
    ...state.position,
    right: horizontal_position === 2,
    bottom: vertical_position === 2,
    edgePadding: margin
  }
  state.JWTversion = JWTjson !== null && JWTjson.FsVersion === '3' ? '3' : '2'
  state.JWTjson = JSON.stringify(JWTjson)
  state.JWTjsonPayload = JWTjson !== null && JWTjson.FsVersion === '3'
    ? JSON.stringify(decodeJWT(JWTjson.ticket))
    : null
  state.messengers = messengers
  state.poweredBy = powered_by
  state.transportsSettings = widget_transports_settings
}

export default setWidgetSettings
