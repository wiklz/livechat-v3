import { SettingsState } from '../../../types/storeTypes/settings'
import I18n from '../../../utils/I18n'

interface FormFieldType {
  id: string;
  custom: boolean;
  enabled: boolean;
  required: boolean;
  type?: string;
  name: {
    en: string;
    [key: string]: string;
  };
  customID?: number;
}

const setWidgetSettings = (
  state: SettingsState,
  {
    payload
  }: {
    payload: any;
    type: string;
  }
):void => {
  const {
    chat_title = '',
    show_op_name = true,
    live_chat_avatar_enable = true,
    live_chat_auto_start_enable = false,
    live_chat_auto_start_delay = 0,
    create_chats_before_messages = false,
    chat_color = '#283CFA',
    present_form_rows_online = { show: false, fields: [] },
    present_form_rows_offline = { show: false, fields: [] },
    custom_fields
  } = payload

  state.chatTitle = chat_title
  state.allowFirstMsg = create_chats_before_messages
  state.themeColor = chat_color
  state.headerInfo = {
    ...state.headerInfo,
    name: show_op_name,
    avatar: !!live_chat_avatar_enable
  }
  state.autoOpen = {
    ...state.autoOpen,
    active: live_chat_auto_start_enable,
    delay: `${live_chat_auto_start_delay}`
  }
  state.onlineForm = {
    show: present_form_rows_online.show_form
      ? present_form_rows_online.show_form
      : false,
    fields:
      present_form_rows_online.fields.filter(
        (field: FormFieldType) => !field.custom
      ).length || !present_form_rows_online.fields.length
        ? [
          {
            enabled: true, id: 'name', required: false, custom: false
          },
          {
            enabled: true, id: 'phone', required: false, custom: false
          }
        ]
        : present_form_rows_online.fields
          .filter((field: FormFieldType) => ((field.enabled || !field.custom) && field.custom
            ? (field.customID || field.customID === 0)
                  && custom_fields.find(
                    (f: FormFieldType) => f.customID === field.customID
                  )
            : true))
          .map((f: FormFieldType) => (f.custom
              && custom_fields.find(
                (cf: FormFieldType) => cf.customID === f.customID
              ).id !== f.id
            ? {
              ...f,
              id: custom_fields.find(
                (cf: FormFieldType) => cf.customID === f.customID
              ).id
            }
            : f))
  }
  state.offlineForm = {
    show: present_form_rows_offline.show_form
      ? present_form_rows_offline.show_form
      : false,
    formName: present_form_rows_offline.form_title
      ? present_form_rows_offline.form_title
      : I18n.t(
        'new_widget_livechat.settings.onlineChat.forms.defaultValues.offline'
      ),
    description: present_form_rows_offline.form_description
      ? present_form_rows_offline.form_description
      : I18n.t(
        'new_widget_livechat.settings.onlineChat.forms.defaultValues.operatorsOffline'
      ),
    fields:
      present_form_rows_offline.fields.filter(
        (field: FormFieldType) => !field.custom
      ).length || !present_form_rows_offline.fields.length
        ? [
          {
            enabled: true, id: 'name', required: false, custom: false
          },
          {
            enabled: true, id: 'phone', required: false, custom: false
          }
        ]
        : present_form_rows_offline.fields
          .filter((field: FormFieldType) => ((field.enabled || !field.custom) && field.custom
            ? (field.customID || field.customID === 0)
                  && custom_fields.find(
                    (f: FormFieldType) => f.customID === field.customID
                  )
            : true))
          .map((f: FormFieldType) => (f.custom
              && custom_fields.find(
                (cf: FormFieldType) => cf.customID === f.customID
              ).id !== f.id
            ? {
              ...f,
              id: custom_fields.find(
                (cf: FormFieldType) => cf.customID === f.customID
              ).id
            }
            : f))
  }
}

export default setWidgetSettings
