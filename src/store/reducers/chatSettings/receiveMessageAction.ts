import ChatMessage from '../../../models/ChatMessage'
import { ChatState } from '../../../types/storeTypes/chat'
import sortMessagesByDate from '../../../utils/sortMessagesById'

interface Attachment {
  id: number
  link: string
  original_file_name: null | string
  content_type: string
  file_size?: number
}

const receiveMessageAction = (
  state: ChatState,
  {
    payload
  }: {
    payload: any
    type: string
  }
) => {
  const {
    id, type, text, date, attachments, status, extra_data = {}
  } = payload

  let modifiedMessages: ChatMessage[] = []

  if (state.messages.find((m) => m.id === id)) {
    const msgs = state.messages.map((m) => (m.id === id
      ? {
        ...m,
        attachments: [
          ...m.attachments,
          ...attachments.map((att: Attachment) => {
            const attachment = { ...att, size: att.file_size }
            delete attachment.file_size
            return attachment
          })
        ]
      }
      : m))
    modifiedMessages = sortMessagesByDate(msgs)
  } else {
    modifiedMessages = sortMessagesByDate([
      new ChatMessage(
        id,
        type,
        text,
        date || Date.parse(`${new Date()}`),
        attachments.map((att: Attachment) => {
          const attachment = { ...att, size: att.file_size }
          delete attachment.file_size
          return attachment
        }),
        status,
        false
      ),
      ...state.messages
    ])
  }

  const new_messages = modifiedMessages.reduce((arr: number[], msg: ChatMessage) => {
    if (msg.type !== 1 && !msg.read) {
      return [...arr, msg.id]
    }
    return arr
  }, [])

  if (extra_data.keyboard) {
    let buttons: {
      text: string;
      type: string;
      [key: string]: any;
    }[] = []
    if (extra_data.keyboard.buttons) {
      buttons = extra_data.keyboard.buttons
    }
    const newOptions = buttons.map((btn, index) => ({
      id: index + 1,
      title: btn.text,
      action: btn.text
    }))

    state.botMenu.show = true
    state.botMenu.options = newOptions
  }

  if (!state.isMuted && !window.chat24_isMobile) {
    new Audio(`${window.chat24_url}/audio/bip.mp3`).play()
  }

  state.messages = modifiedMessages
  state.new_messages = new_messages
}

export default receiveMessageAction
