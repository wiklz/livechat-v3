import { createSlice } from '@reduxjs/toolkit'
import { ChatState } from '../../../types/storeTypes/chat'
import receiveMessageAction from './receiveMessageAction'

const initialState: ChatState = {
  jwtSaved: false,
  client_phone: null,
  activeChannel: null,
  messages: [],
  new_messages: [],
  showEmoji: false,
  messageInput: {
    text: ''
  },
  isMuted: false,
  user: {
    isSendingAuthForm: false,
    isAuthorized: false,
    name: '',
    phone: ''
  },
  botMenu: {
    show: false,
    options: []
  },
  modals: {
    attachment: {
      show: false,
      file: null,
      url: null,
      name: null,
      type: null,
      size: null,
      text: null
    }
  }
}

const chatSlice = createSlice({
  name: 'chat',
  initialState,
  reducers: {
    setClientPhone: (state, { payload }) => {
      state.client_phone = payload
    },
    setClientAuth: (state, { payload }) => {
      if (
        payload !== null
        && (payload.name
          || payload.phone
          || (payload.custom_fields
            && Object.values(payload.custom_fields).find((val) => Boolean(val))))
      ) {
        state.user.isAuthorized = true
      }
    },
    setClientJwtSaved: (state, { payload }) => {
      state.jwtSaved = payload
    },
    initMessagesSuccess: (state, { payload }) => {
      const { messages, new_messages } = payload
      state.messages = messages
      state.new_messages = new_messages
    },
    changeBotMenu: (state, { payload }) => {
      const { buttons } = payload.extra_data.keyboard

      state.botMenu.options = buttons.map(
        (btn: { text: string }, index: number) => ({
          id: index + 1,
          title: btn.text,
          action: btn.text,
          icon: null
        })
      )
      state.botMenu.show = true
    },
    receiveMessage: receiveMessageAction
  }
})
export default chatSlice.reducer
export const {
  setClientPhone,
  setClientAuth,
  setClientJwtSaved,
  initMessagesSuccess,
  changeBotMenu,
  receiveMessage
} = chatSlice.actions
