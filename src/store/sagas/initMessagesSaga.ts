import { select, put } from 'redux-saga/effects'
import axios from 'axios'
import { RootState } from '..'
import {
  changeChatCenterStatus,
  changeOperatorInfo
} from '../reducers/settingsReducer'
import {
  initMessagesSuccess,
  changeBotMenu
} from '../reducers/chatSettings'
import ChatMessage from '../../models/ChatMessage'

interface Attachment {
  id: number;
  link: string;
  original_file_name: null | string;
  content_type: string;
  file_size?: number;
}

interface ResponseMessage {
  id: number;
  type: number;
  text: string;
  date: number;
  from: number;
  status: number;
  read: number;
  attachments: Attachment[];
  extra_data: {
    url?: string;
    utm?: string;
    google_id?: null | number;
    yandex_id?: null | number;
    comagic_id?: null | number;
    roistat_visit?: any;
    keyboard?: any;
  };
}

const getState = (state: RootState) => state

function* initMessagesSaga (): Generator<any, void, any> {
  const state = yield select(getState)
  let msgWithKeyboard = yield null

  try {
    const initMessagesData = yield axios(
      `${window.chat24_url}/messages?client_id=${state.chat.client_phone}&widget_token=${window.chat24_token}`
    )

    yield put(changeChatCenterStatus(initMessagesData.data.chat_center.status))

    const messages: ChatMessage[] = yield initMessagesData.data.messages
      ? initMessagesData.data.messages.map((msg: ResponseMessage) => {
        if (msgWithKeyboard === null && msg.extra_data) {
          if (msg.extra_data.keyboard) {
            msgWithKeyboard = msg
          }
        }
        return new ChatMessage(
          msg.id,
          msg.type,
          msg.text,
          msg.date * 1000,
          msg.attachments
            ? msg.attachments.map((item) => {
              const att = { ...item, size: item.file_size }
              delete att.file_size
              return att
            })
            : [],
          msg.read ? 'read' : 'delivered',
          Boolean(msg.read),
          msg.extra_data
        )
      })
      : []

    const new_messages: number[] = yield messages.reduce(
      (arr: number[], msg: ChatMessage) => {
        if (msg.type !== 1 && !msg.read) {
          return [...arr, msg.id]
        }
        return arr
      },
      []
    )

    yield put(initMessagesSuccess({ messages, new_messages }))

    if (initMessagesData.data.operator) {
      yield put(
        changeOperatorInfo({
          ...initMessagesData.data.operator,
          url_prefix: window.chat24_static_files_domain
        })
      )
    }

    if (messages.length) {
      const message = messages[0]
      if (message.type !== 1 && message.extra_data && message.extra_data.keyboard) {
        yield put(changeBotMenu(msgWithKeyboard))
      }
    }
  } catch (err) {
    yield console.error('Could not get saved messages: ', err)
  }
}

export default initMessagesSaga
