import { eventChannel } from 'redux-saga'
import {
  put, call, take, select
} from 'redux-saga/effects'
import { RootState } from '..'
import messageStatuses from '../../consts/messageStatuses'
import { receiveMessage } from '../reducers/chatSettings'
import { changeOperatorInfo, changeChatCenterStatus, changeOperatorTyping } from '../reducers/settingsReducer'

const getState = (state: RootState) => state

let socketstate: boolean | undefined
let send_interval: number | undefined

function createEventChannel (ws: WebSocket) {
  return eventChannel((emit) => {
    ws.onopen = () => {
      socketstate = true

      console.log('Online chat sockets are connected 😄')

      send_interval = window.setInterval(() => {
        ws.send(JSON.stringify({ action_name: 'ping' }))
      }, 2000)
    }

    ws.onclose = () => {
      if (socketstate) {
        socketstate = false
        window.clearInterval(send_interval)
      }

      console.log('webSocketSaga disconnected 🥶')

      // reconnection logic
      //   eslint-disable-next-line
      window.setTimeout(() => {
        if (!socketstate) {
          console.log('webSocketSaga reconnecting 😇')

          return emit({
            type: 'chat/initSocketConnection'
          })
        }
      }, 3000)
    }

    ws.onerror = (e) => console.log('error in webSocketSaga 😡: ', e)

    // eslint-disable-next-line
    ws.onmessage = (res) => {
      if (JSON.parse(res.data) && (JSON.parse(res.data) !== 'pong' || JSON.parse(res.data).action_name)) {
        const socketMsg = JSON.parse(res.data)

        if (socketMsg.action_name === 'message') {
          return emit(receiveMessage({
            id: socketMsg.data.id,
            type: socketMsg.data.type || messageStatuses.out,
            text: socketMsg.data.text,
            date: Date.parse(`${new Date(socketMsg.data.sent_at * 1000)}`),
            attachments: socketMsg.data.attachments,
            status: socketMsg.data.read === 1 ? 'read' : 'delivered',
            extra_data: socketMsg.data.extra_data
          }))
        }

        if (socketMsg.action_name === 'change operator') {
          return emit(changeOperatorInfo({
            ...socketMsg.data,
            url_prefix: window.chat24_static_files_domain
          }))
        }

        if (socketMsg.action_name === 'change chat_center') {
          return emit(changeChatCenterStatus(socketMsg.data.status))
        }

        if (socketMsg.action_name === 'change operator_typing') {
          return emit(changeOperatorTyping(socketMsg.data.status))
        }
      }
    }

    return () => {
      ws.close()
    }
  })
}

function* webSocketSaga (): Generator<any, void, any> {
  const state = yield select(getState)

  const ws = new WebSocket(`${window.chat24_socket_url}?widget_token=${window.chat24_token}&client_id=${state.chat.client_phone}`)

  const channel = yield call(createEventChannel, ws)

  const action = yield take(channel)

  yield put(action)
}

export default webSocketSaga
