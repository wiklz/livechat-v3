import { put } from 'redux-saga/effects'
import Cookies from 'js-cookie'
import PostApi from '../utils/PostAPI'
import I18n from '../../../utils/I18n'
import getMessengers from '../../../utils/getMessengers'
import {
  setWidgetSettings,
  setChatSettings,
  initSuccess
} from '../../reducers/settingsReducer'
import { setClientPhone } from '../../reducers/chatSettings'

interface FormFieldType {
  id: string;
  custom: boolean;
  enabled: boolean;
  required: boolean;
  type?: string;
  name: {
    en: string;
    [key: string]: string;
  };
}

function* initWithoutCookie (): Generator<any, void, any> {
  try {
    const start_response = yield PostApi(
      `${window.chat24_url}/start?id=${window.chat24_token}&lang=${window.lang}`
    )

    yield I18n.load(start_response.data.locales)

    const client_phone = yield start_response.data.client_key

    yield put(
      setWidgetSettings({
        data: { ...start_response.data.widget_settings },
        messengers: getMessengers(
          start_response.data.widget_enabled_transports.map(
            (transport: string) => start_response.data.transports_full_data.find(
              (tr: { type: string; url?: string }) => tr.type === transport
            )
          )
        ),
        active_channel: start_response.data.active_channel,
        url_tracking: start_response.data.url_tracking,
        chat_center: start_response.data.chat_center
      })
    )

    yield put(
      setChatSettings({
        ...start_response.data.widget_settings,
        custom_fields: start_response.data.custom_fields
          ? start_response.data.custom_fields
            .filter((f: FormFieldType) => f.type === 'input')
            .reduce(
              (arr: FormFieldType[], field: FormFieldType) => [
                ...arr,
                {
                  customID: field.id,
                  id: field.name[window.lang] || field.name.en
                }
              ],
              []
            )
          : []
      })
    )

    yield Cookies.set(
      'c2d_widget_id',
      JSON.stringify({
        [window.chat24_token]: client_phone
      }),
      {
        expires: (() => {
          const d = new Date()
          d.setMonth(d.getMonth() + 12)
          return d
        })()
      }
    )

    yield put(setClientPhone(client_phone))

    yield put(initSuccess())
  } catch (err) {
    console.log('Failed to initialize widget!', err)
  }
}

export default initWithoutCookie
