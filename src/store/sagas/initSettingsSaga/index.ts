import { call } from 'redux-saga/effects'
import Cookies from 'js-cookie'
import initWithJWT from './initWithJWT'
import initWithoutCookie from './initWithoutCookie'
import initWithCookie from './initWithCookie'

function* initSettingsSaga (): Generator<any, void, any> {
  if (window.JWT === 'enabled') {
    yield call(initWithJWT)
  } else if (!Cookies.get('c2d_widget_id')) {
    yield call(initWithoutCookie)
  } else {
    yield call(initWithCookie)
  }
}

export default initSettingsSaga
