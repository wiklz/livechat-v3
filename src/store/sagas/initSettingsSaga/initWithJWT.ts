import { put } from 'redux-saga/effects'
import { decodeJWT, isJWTvalid } from '../../../utils/JWT'
import getMessengers from '../../../utils/getMessengers'
import PostApi from '../utils/PostAPI'
import {
  setClientPhone,
  setClientAuth,
  setClientJwtSaved
} from '../../reducers/chatSettings'
import {
  setWidgetSettings,
  setChatSettings,
  initSuccess
} from '../../reducers/settingsReducer'

interface FormFieldType {
  id: string;
  custom: boolean;
  enabled: boolean;
  required: boolean;
  type?: string;
  name: {
    en: string;
    [key: string]: string;
  };
}

function* initWithJWT (): Generator<any, void, any> {
  try {
    const path = window.JWT_payload_userId_path.split(':')
    let jwtJson: any
    // let payload: any
    try {
      const JWTinput = document.getElementById(
        window.JWT_hidden_field_id
      ) as HTMLInputElement | null

      if (JWTinput) {
        jwtJson = JSON.parse(JWTinput.value)
      }
    } catch (error) {
      console.log(error)
      console.log(
        'JWT is not found, session for the user can not be initialized.'
      )
      return
    }

    let clientId: any
    const payload:{ id: string; jwt?: string; client_key?: string | number } = { id: window.chat24_token }
    let prefixChannel = 'app'
    if (jwtJson.FsVersion === '2') {
      const token = decodeJWT(jwtJson.token)
      prefixChannel = token.aud === 'OneApp' ? 'app' : 'web'
    }
    if (jwtJson.FsVersion === '3') {
      const jwtValid = yield isJWTvalid(jwtJson)
      if (yield jwtValid.data !== true) {
        console.log(
          'JWT is not valid, session for the user can not be initialized.'
        )
        return
      }
      const jwtPayload = decodeJWT(jwtJson.ticket)
      clientId = jwtPayload
      prefixChannel = jwtPayload.aud === 'OneApp' ? 'app' : 'web'
      path.forEach((p) => {
        clientId = clientId[p]
      })
    } else {
      clientId = jwtJson
      path.forEach((p) => {
        clientId = clientId[p]
      })
      payload.jwt = jwtJson.ticket
    }
    clientId = `[${prefixChannel}][chat] ${clientId}`
    payload.client_key = clientId

    const start_response = yield PostApi(`${window.chat24_url}/start`, payload)
    if (jwtJson.FsVersion !== '3' && !start_response.data.jwt_valid) {
      console.log(
        'JWT is not valid, session for the user can not be initialized.'
      )
      return
    }

    yield put(setClientPhone(clientId))

    yield put(setClientAuth(start_response.data.client))

    yield put(
      setClientJwtSaved(
        start_response.data.client.custom_fields !== undefined
          && start_response.data.client.extra_comment_1 === JSON.stringify(jwtJson)
      )
    )

    yield put(
      setWidgetSettings({
        data: { ...start_response.data.widget_settings },
        messengers: getMessengers(
          start_response.data.widget_enabled_transports.map(
            (transport: string) => start_response.data.transports_full_data.find(
              (tr: { type: string; url?: string }) => tr.type === transport
            )
          )
        ),
        active_channel: start_response.data.active_channel,
        url_tracking: start_response.data.url_tracking,
        chat_center: start_response.data.chat_center
      })
    )

    yield put(
      setChatSettings({
        ...start_response.data.widget_settings,
        custom_fields: start_response.data.custom_fields
          ? start_response.data.custom_fields
            .filter((f: FormFieldType) => f.type === 'input')
            .reduce(
              (arr: FormFieldType[], field: FormFieldType) => [
                ...arr,
                {
                  customID: field.id,
                  id: field.name[window.lang] || field.name.en
                }
              ],
              []
            )
          : []
      })
    )

    yield put(initSuccess())
    return
  } catch (error) {
    console.log(error)
    console.log('User can not be recognized using JWT')
  }
}

export default initWithJWT
