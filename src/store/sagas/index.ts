import { all, takeLatest } from 'redux-saga/effects'
import initSettingsSaga from './initSettingsSaga'
import initMessagesSaga from './initMessagesSaga'
import webSocketSaga from './webSocketSaga'

export default function* sagas (): Generator<any, void, any> {
  yield all([
    takeLatest('widget/initSettings', initSettingsSaga),
    takeLatest('chat/initMessages', initMessagesSaga),
    takeLatest('chat/initSocketConnection', webSocketSaga)
  ])
}
