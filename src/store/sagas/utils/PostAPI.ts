import { CANCEL } from 'redux-saga'
import axios from 'axios'

export default function fetchAPI (url: string, payload?: any): any {
  const source = axios.CancelToken.source()
  const request: any = axios.post(url, payload, {
    cancelToken: source.token
  })
  request[CANCEL] = () => source.cancel()
  return request
}
