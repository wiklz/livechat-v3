import React from 'react'

interface Props {
  isMobile: boolean;
}

const OnlineChat: React.FC<Props> = () => <div>OnlineChat container</div>

export default OnlineChat
