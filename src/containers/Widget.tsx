import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Transition from 'react-transition-group/Transition'
import MessengersList from '../components/MessengersList'
import StartBtn from '../components/StartBtn'
import WidgetContainer from '../hocs/WidgetContainer'
import { RootState } from '../store'
import { setWidgetOpen } from '../store/reducers/applicationSettings'
import customEvent from '../utils/CustomEvent'

interface Props {
  isMobile: boolean
}

const Widget: React.FC<Props> = ({ isMobile }: Props) => {
  const messengersRef = useRef<HTMLDivElement>(null)

  const dispatch = useDispatch()

  const initIsCollapsedValue = useSelector((state: RootState) => state.settings.appearance[isMobile ? 'mobile' : 'desktop'].collapsed)
  const isCollapsed = useSelector((state: RootState) => !state.application.widgetOpen)

  useEffect(() => {
    document.dispatchEvent(customEvent('initialize_widget'))
  }, [])

  useEffect(() => {
    dispatch(setWidgetOpen(!initIsCollapsedValue))
  }, [dispatch, initIsCollapsedValue])

  return (
    <>
      {isMobile && <StartBtn isMobile isOutside />}
      <WidgetContainer isMobile={isMobile}>
        <Transition nodeRef={messengersRef} in={!isCollapsed} timeout={0} mountOnEnter={false}>
          {(transitionState) => <MessengersList ref={messengersRef} isMobile={isMobile} transitionState={transitionState} />}
        </Transition>
        <StartBtn isMobile={isMobile} isOutside={false} />
      </WidgetContainer>
    </>
  )
}

export default Widget
