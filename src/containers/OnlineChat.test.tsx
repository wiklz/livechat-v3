import React from 'react'
import { render, screen } from '@testing-library/react'
import OnlineChat from './OnlineChat'

test('renders online chat container', () => {
  render(<OnlineChat isMobile={false} />)
  const linkElement = screen.getByText(/onlineChat container/i)
  expect(linkElement).toBeInTheDocument()
})
