import React, { useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Style } from '../../lib/react-shade'
import { RootState } from '../../store'
import Badge from '../UI/Badge'
import { toggleWidgetOpen } from '../../store/reducers/applicationSettings'

interface Props {
  isMobile: boolean
  isOutside: boolean
}

const StartBtn: React.FC<Props> = ({ isMobile, isOutside }: Props) => {
  const theme = useSelector((state: RootState) => state.settings.theme)
  const isCollapsed = useSelector((state: RootState) => !state.application.widgetOpen)
  const icon = useSelector((state: RootState) => state.settings.startBtn.active)
  const url = useSelector((state: RootState) => state.settings.startBtn.buttons[4].url)
  const color = useSelector((state: RootState) => state.settings.startBtn.color)
  const newMessagesCount = useSelector((state: RootState) => state.chat.new_messages.length)
  const themeColor = useSelector((state: RootState) => state.settings.themeColor)
  const right = useSelector((state: RootState) => state.settings.position.right)
  const bottom = useSelector((state: RootState) => state.settings.position.bottom)
  const horizontal = useSelector((state: RootState) => state.settings.appearance[isMobile ? 'mobile' : 'desktop'].horizontal)

  const dispatch = useDispatch()
  const toggleWidget = () => dispatch(toggleWidgetOpen())

  const closeBtnBackground = useMemo(() => {
    const bg = theme === 'light'
      ? {
        red: parseInt(themeColor.slice(1, 3), 16),
        green: parseInt(themeColor.slice(3, 5), 16),
        blue: parseInt(themeColor.slice(5, 7), 16)
      }
      : {
        red: 255,
        green: 255,
        blue: 255
      }

    const alpha = theme === 'light' ? '0.05' : '0.1'

    return `rgba(${bg.red}, ${bg.green}, ${bg.blue}, ${alpha})`
  }, [theme, themeColor])

  const btnImg = useMemo(
    () => (icon === 4 ? (
      <img className="startBtn__img" src={url} alt="priview-start-btn-mob" />
    ) : (
      <svg height="30" viewBox={icon === 1 ? '0 0 22 20' : '0 0 20 20'}>
        <use xlinkHref={`#svg-type-${icon}`} />
      </svg>
    )),
    [icon, url]
  )

  const classes = useMemo(() => {
    const appliedClasses = []

    if (isMobile && isOutside) {
      appliedClasses.push('startBtn--mobile')
    } else {
      appliedClasses.push('startBtn')
    }

    if (icon !== 4 || !isOutside) {
      appliedClasses.push(`startBtn--${theme}`)
      appliedClasses.push(`startBtn--${horizontal ? 'horizontal' : 'vertical'}`)
    }
    appliedClasses.push(`startBtn--${right ? 'right' : 'left'}`)
    appliedClasses.push(`startBtn--${bottom ? 'bottom' : 'center'}`)

    appliedClasses.push(`startBtn--${isCollapsed ? 'collapsed' : 'open'}`)

    return appliedClasses.join(' ')
  }, [theme, isCollapsed, isOutside, isMobile, right, bottom, icon, horizontal])

  const sharedStyles = {
    '.startBtn': {
      zIndex: 1,
      border: 'none',
      outline: 'none',
      boxShadow: 'none',
      padding: 0,
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      overflow: 'hidden',
      position: 'relative'
      // backgroundColor: 'transparent'
    },
    '.startBtn--dark': {
      backgroundColor: '#0e1621'
    },
    '.startBtn--light': {
      backgroundColor: '#fff'
    },
    '.startBtn--close': {
      backgroundColor: 'transparent'
    },
    '.startBtn__img': {
      width: '100%',
      height: '100%',
      margin: 0
    }
  }

  const desktopStyles = {
    ...sharedStyles,
    '.startBtn': {
      ...sharedStyles['.startBtn'],
      width: '74px',
      height: '74px',
      borderRadius: '37px',
      cursor: 'pointer'
    },
    '.strtBtn:hover': {
      backgroundColor: 'transparent'
    },
    '.startBtn--open': {
      width: '54px',
      height: '54px',
      margin: 'auto',
      borderRadius: '6px'
    },
    '.startBtn--open:hover': {
      backgroundColor: closeBtnBackground
    },
    '.startBtn--open svg': {
      transition: 'all 0.1s linear'
    },
    '.startBtn--open:hover svg': {
      transform: 'scale(1.15)'
    }
  }

  const mobileStyles = {
    ...sharedStyles,
    '.startBtn--mobile': {
      ...sharedStyles['.startBtn'],
      position: 'fixed',
      boxShadow: '0px 5px 25px rgb(0 0 0 / 15%)',
      width: '74px',
      height: '74px',
      borderRadius: '37px',
      transition: 'all 0.2s linear'
    },
    '.startBtn--mobile.startBtn--left': {
      left: '12px'
    },
    '.startBtn--mobile.startBtn--right': {
      right: '12px'
    },
    '.startBtn--mobile.startBtn--bottom': {
      bottom: '12px'
    },
    '.startBtn--mobile.startBtn--center': {
      bottom: 'calc(100vh / 2 - 32px)'
    },
    '.startBtn--mobile.startBtn--open.startBtn--center.startBtn--left': {
      transform: 'translateX(-86px)'
    },
    '.startBtn--mobile.startBtn--open.startBtn--center.startBtn--right': {
      transform: 'translateX(86px)'
    },
    '.startBtn--mobile.startBtn--open.startBtn--bottom': {
      transform: 'translateY(86px)'
    },
    '.startBtn.startBtn--horizontal': {
      height: '100%',
      width: '54px'
    },
    '.startBtn.startBtn--vertical': {
      width: '100%',
      height: '54px'
    }
  }

  const styles = useMemo(() => (isMobile ? mobileStyles : desktopStyles), [isMobile])

  const showIcon = useMemo(() => {
    if (isMobile) {
      return isOutside
    }
    return isCollapsed
  }, [isMobile, isOutside, isCollapsed])

  return (
    <>
      <Style>{styles}</Style>
      <button type="button" style={{ color }} className={classes} onClick={toggleWidget}>
        {showIcon ? (
          btnImg
        ) : (
          <svg width="14" height="14" viewBox="0 0 22 22">
            <use xlinkHref={`#svg-type-close-${theme}`} />
          </svg>
        )}
      </button>
      {isCollapsed && newMessagesCount ? <Badge>{`${newMessagesCount}`}</Badge> : null}
    </>
  )
}

export default StartBtn
