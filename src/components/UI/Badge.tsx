import React, { useMemo } from 'react'
import { Style } from '../../lib/react-shade'

interface Props {
    top?: number;
    right?: number;
    children: any;
}

const Badge: React.FC<Props> = ({ children, top = 0, right = 0 }:Props) => {
  const styles = useMemo(() => ({
    '.badge': {
      position: 'absolute',
      top,
      right,
      display: 'flex',
      opacity: 0,
      justifyContent: 'center',
      alignItems: 'center',
      height: '19px',
      padding: '0 6.75px',
      backgroundColor: '#f53a11',
      borderRadius: '10px',
      fontFamily: '"Roboto", sans-serif',
      fontWeight: 700,
      fontSize: '10px',
      lineHeight: '9px',
      color: '#ffffff',
      zIndex: 15,
      animation: 'fade 0.2s linear forwards'
    }
  }), [top, right])

  return (
    <>
      <Style>{styles}</Style>
      <span className="badge">
        {children}
      </span>
    </>
  )
}

export default Badge
