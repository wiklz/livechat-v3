import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'
// import buildRef from '../../../utils/buildTransportUrl'
import Phone from './Phone'
import Default from './Default'
import { RootState } from '../../../store'

interface Props {
  isMobile: boolean
  is_phone: boolean
  messenger?: {
    id: string
    url?: string | undefined
    dbId: number
    name: string
  }
  makeCall: () => void
}

const Messenger: React.FC<Props> = ({
  isMobile, is_phone, messenger, makeCall
}: Props) => {
  const activeChannel = useSelector((state: RootState) => state.settings.activeChannel)
  const horizontal = useSelector((state: RootState) => state.settings.appearance[isMobile ? 'mobile' : 'desktop'].horizontal)
  const theme = useSelector((state: RootState) => state.settings.theme)
  const urlTracking = useSelector((state: RootState) => state.settings.urlTracking)
  const newMessagesCount = useSelector((state: RootState) => state.chat.new_messages.length)
  const transportSetting = useSelector((state: RootState) => state.settings.transportsSettings)
  const right = useSelector((state: RootState) => state.settings.position.right)
  const bottom = useSelector((state: RootState) => state.settings.position.bottom)
  const edgePadding = useSelector((state: RootState) => state.settings.position.edgePadding)

  const handleClick = useCallback(() => {
    // TODO: make this work
    // if (messenger.id.match(/viber/)) {
    //   toggleViber({
    //     url: buildRef(messenger, urlTracking, activeChannel),
    //     transport: messenger.name
    //   })
    // } else if (messenger.id === 'widget') {
    //   toggleChat()
    // } else if (messenger.id === 'yandex_dialogs') {
    //   handleYandexClick()
    // } else if (messenger.url) {
    //   close()
    //   window.open(buildRef(messenger, urlTracking, activeChannel), '_blank')
    // }
    console.log(activeChannel, urlTracking)
  }, [])

  return !is_phone && messenger ? (
    <Default
      messengerId={messenger.id}
      horizontal={horizontal}
      newMessagesCount={newMessagesCount}
      theme={theme}
      handleClick={handleClick}
      transportSetting={transportSetting}
      right={right}
      bottom={bottom}
      edgePadding={edgePadding}
    />
  ) : (
    <Phone horizontal={horizontal} theme={theme} makeCall={makeCall} />
  )
}

export default Messenger
