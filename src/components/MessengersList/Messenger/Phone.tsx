import React from 'react'

interface Props {
    horizontal: boolean;
    theme: 'light' | 'dark';
    makeCall: () => void;
}

const PhoneBtn: React.FC<Props> = ({ horizontal, theme, makeCall }: Props) => (
  <button
    type="button"
    className={`messenger messenger--${theme} messenger--${horizontal ? 'horizontal' : 'vertical'}`}
    onClick={makeCall}
  >
    <svg width="34" height="34" viewBox="0 0 40 40">
      <use xlinkHref={`#phone-${theme}`} />
    </svg>
  </button>
)

export default PhoneBtn
