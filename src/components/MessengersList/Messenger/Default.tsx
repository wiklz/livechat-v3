import React, { useCallback, useMemo } from 'react'
import Badge from '../../UI/Badge'

interface Props {
    horizontal: boolean;
    right:boolean;
    bottom: boolean;
    edgePadding:number;
    newMessagesCount: number;
    messengerId: string;
    theme: 'light' | 'dark';
    transportSetting: {
      yandex_dialogs?: {
          code: string;
      } | undefined;
  } | null | undefined
    handleClick: () => void;
}

const getSvgName = (messengerId: string) => {
  switch (messengerId) {
    case 'whatsapp':
    case 'wa_clickatell':
    case 'wa_infobip':
    case 'wa_cm':
    case 'wa_tr':
    case 'wa_wavy':
    case 'wa_botmaker':
    case 'wa_dialog':
      return 'whatsapp'

    case 'telegram':
    case 'tg_user':
      return 'telegram'

    case 'viber':
    case 'viber_tr':
    case 'viber_public':
    case 'viber_business':
    case 'viber_infobip':
      return 'viber'

    case 'instagram':
    case 'insta_i2crm':
    case 'insta_comment':
    case 'insta_local':
      return 'instagram'

    case 'system':
    case 'modem':
    case 'external':
      return 'external'

    default:
      return messengerId
  }
}

const Default: React.FC<Props> = ({
  horizontal,
  newMessagesCount,
  messengerId,
  theme,
  transportSetting,
  right,
  bottom,
  edgePadding,
  handleClick
}:Props) => {
  const renderYandexDialogs = (code: string) => {
    if (!document.querySelector('#yandex_dialogs_script')) {
      const scriptTag = document.createElement('script')
      const innerText = code
        .replace(/^(<script(.+)?>)/, '')
        .replace(/(<\/script>)$/, '')
      scriptTag.id = 'yandex_dialogs_script'
      scriptTag.innerText = innerText
      document.body.appendChild(scriptTag)
    }

    const interval = window.setInterval(() => {
      const yandexWidget = document.querySelector('.ya-chat-widget')
      if (yandexWidget) {
        window.clearInterval(interval)
        /* eslint-disable max-len */
        /* eslint-disable @typescript-eslint/ban-ts-comment */
        // @ts-ignore
        if (!yandexWidget.style.display) {
          // @ts-ignore
          yandexWidget.style = 'display: none !important;'

          const positionStyles = horizontal
            ? `
            ${right ? `right: ${edgePadding}vw !important; left: auto !important; ` : `right: auto !important; left: ${edgePadding}vw !important; `}
            ${bottom ? `bottom: calc(${edgePadding}vh + 74px) !important; top: auto !important; ` : `bottom: auto !important; top: calc(50vh + 37px) !important; max-height: calc(50vh - ${edgePadding}vh - 74px) !important; `}
          `
            : `
            ${right ? `right: calc(${edgePadding}vw + 74px) !important; left: auto !important; ` : `right: auto !important; left: calc(${edgePadding}vw + 74px) !important; `}
            ${bottom ? `bottom: ${edgePadding}vh !important; top: auto !important; ` : `bottom: auto !important; top: calc(50vh - 37px) !important; max-height: calc(50vh - ${edgePadding}vh) !important; `}
          `

          // @ts-ignore
          document.querySelector('.ya-chat-popup__content-wrapper').style = `${positionStyles} position: fixed !important; box-shadow: 0px 5px 25px rgba(0, 0, 0, 0.15) !important; `
          // @ts-ignore
          document.querySelector('.ya-chat-header__close').onclick = () => handleYandexClick('close_button')
        }

        /* eslint-enable @typescript-eslint/ban-ts-comment */
        /* eslint-enable max-len */
      }
    }, 50)
  }

  const handleTransportSettings = useCallback((transportType: string) => {
    const code = transportSetting && transportSetting.yandex_dialogs ? transportSetting.yandex_dialogs.code : null
    switch (transportType) {
      case 'yandex_dialogs':
        return code ? renderYandexDialogs(code) : null

      default:
        return null
    }
  }, [transportSetting, renderYandexDialogs])

  const classes = useMemo(() => (
    ['messenger', `messenger--${theme}`, `messenger--${horizontal ? 'horizontal' : 'vertical'}`].join(' ')
  ), [theme, horizontal])

  return (
    <>
      <button type="button" className={classes} onClick={handleClick}>
        <svg width="34" height="34" viewBox="0 0 40 40">
          <use xlinkHref={`#${getSvgName(messengerId)}-${theme}`} />
        </svg>
        {newMessagesCount && messengerId === 'widget' ? (
          <Badge top={6} right={6}>
            {newMessagesCount}
          </Badge>
        ) : null}
      </button>
      {transportSetting ? handleTransportSettings(messengerId) : null}
    </>
  )
}

export default Default
