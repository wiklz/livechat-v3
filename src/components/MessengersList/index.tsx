import React, { useCallback, useMemo } from 'react'
import { TransitionStatus } from 'react-transition-group'
import { useSelector } from 'react-redux'
import { Style } from '../../lib/react-shade'
import { RootState } from '../../store'
import Messenger from './Messenger'

interface Props {
  isMobile: boolean
  transitionState: TransitionStatus
}

const MESSENGER_ICON_DIMENSIONS = 54

const MessengersList = React.forwardRef<HTMLDivElement, Props>(({ isMobile, transitionState }: Props, ref) => {
  const horizontal = useSelector((state: RootState) => state.settings.appearance[isMobile ? 'mobile' : 'desktop'].horizontal)
  const messengers = useSelector((state: RootState) => state.settings.messengers)
  const transportsSettings = useSelector((state: RootState) => state.settings.transportsSettings)
  const is_phone = useSelector((state: RootState) => state.settings.is_phone)
  const theme = useSelector((state: RootState) => state.settings.theme)
  const themeColor = useSelector((state: RootState) => state.settings.themeColor)
  const phone_type = useSelector((state: RootState) => state.settings.phone_type)
  const phone_number = useSelector((state: RootState) => state.settings.phone_number)
  const phone_sip_link = useSelector((state: RootState) => state.settings.phone_sip_link)

  const makeCall = () => {
    if (phone_type === 0) {
      window.open(`tel:${phone_number}`, '_blank')
    }
    if (phone_type === 1) {
      window.open(phone_sip_link, '_blank')
    }
  }

  const classes = useMemo(() => {
    const appliedClasses = ['messengers']

    if (isMobile) {
      appliedClasses.push('messengers--mobile')
    }

    if (horizontal) {
      appliedClasses.push('messengers--horizontal')
    } else {
      appliedClasses.push('messengers--vertical')
    }

    appliedClasses.push(`messengers--${transitionState}`)

    return appliedClasses.join(' ')
  }, [horizontal, transitionState])

  const shouldShowMessenger = (id: string) => {
    switch (id) {
      case 'yandex_dialogs':
        return transportsSettings && transportsSettings.yandex_dialogs && transportsSettings.yandex_dialogs.code

      default:
        return true
    }
  }

  const getDimenions = (type: 'width' | 'height') => {
    if (!horizontal && type === 'width') {
      return '100%'
    }
    if (horizontal && type === 'height') {
      return '100%'
    }

    const noPhone = messengers.filter((msg) => shouldShowMessenger(msg.id)).length * MESSENGER_ICON_DIMENSIONS
    const withPhone = noPhone + MESSENGER_ICON_DIMENSIONS

    switch (transitionState) {
      case 'exited':
        return 0

      default:
        return is_phone ? withPhone : noPhone
    }
  }

  const btnBackground = useMemo(() => {
    const bg = theme === 'light'
      ? {
        red: parseInt(themeColor.slice(1, 3), 16),
        green: parseInt(themeColor.slice(3, 5), 16),
        blue: parseInt(themeColor.slice(5, 7), 16)
      }
      : {
        red: 255,
        green: 255,
        blue: 255
      }

    const alpha = theme === 'light' ? '0.05' : '0.1'

    return `rgba(${bg.red}, ${bg.green}, ${bg.blue}, ${alpha})`
  }, [theme, themeColor])

  const sharedStyles = {
    '.messengers': {
      display: 'flex',
      overflow: 'hidden',
      transition: 'all 0.1s linear'
    },
    '.messengers--horizontal': {
      flexFlow: 'row'
    },
    '.messengers--vertical': {
      flexFlow: 'column'
    },
    '.messenger': {
      border: 'none',
      outline: 'none',
      boxShadow: 'none',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      padding: '0',
      backgroundColor: 'transparent',
      position: 'relative',
      transition: 'all 0.1s linear'
    }
  }

  const desktopStyles = useMemo(
    () => ({
      ...sharedStyles,
      '.messengers--exited, .messengers--exiting, .messengers--entering': {
        opacity: '0'
      },
      '.messengers--entered': {
        opacity: '1'
      },
      '.messenger': {
        ...sharedStyles['.messenger'],
        borderRadius: '6px',
        width: '54px',
        height: '54px',
        margin: 'auto',
        cursor: 'pointer'
      },
      '.messenger svg': {
        transition: 'all 0.1s linear'
      },
      '.messenger:hover': {
        backgroundColor: btnBackground
      },
      '.messenger:hover svg': {
        transform: 'scale(1.1)'
      }
    }),
    [btnBackground]
  )

  const mobileStyles = {
    ...sharedStyles,
    '.messenger': {
      ...sharedStyles['.messenger'],
      margin: 'auto'
    },
    '.messengers--horizontal .messenger': {
      width: '50px',
      height: '100%'
    },
    '.messengers--vertical .messenger': {
      height: '50px',
      width: '100%'
    }
  }

  const styles = isMobile ? mobileStyles : desktopStyles

  const getInlineStyles = useCallback(() => {
    if (isMobile) {
      return {}
    }
    return { height: getDimenions('height'), width: getDimenions('width') }
  }, [isMobile, getDimenions])

  return (
    <>
      <Style>{styles}</Style>
      <div ref={ref} className={classes} style={getInlineStyles()}>
        {messengers.map((messenger) => (shouldShowMessenger(messenger.id) ? (
          <Messenger
            key={`controls-messenger-${messenger.id}`}
            is_phone={false}
            isMobile={isMobile}
            makeCall={makeCall}
            messenger={messenger}
          />
        ) : null))}
        {is_phone && <Messenger is_phone isMobile={isMobile} makeCall={makeCall} />}
      </div>
    </>
  )
})

export default MessengersList
