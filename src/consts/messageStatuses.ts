export default {
  in: 1,
  out: 2,
  auto: 3,
  system: 4,
  rating_in: 5,
  rating_out: 6,
  comment: 7
}
