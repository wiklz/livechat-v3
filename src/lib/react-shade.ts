// Fixes the `module` field pointing to `.mjs`.
import * as rs from 'react-shade/dist/index.js'

// eslint-disable-next-line
// @ts-ignore
export default rs.default.default

// Exports everything else
export * from 'react-shade/dist/index.js'
